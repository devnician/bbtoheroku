Laravel Project from Bitbucket Pipeline to Heroku

Pre-requisits:
download heroku-cli

Step 1: Create laravel project and push bitbucket repo

    Create laravel project using this command
        $ composer create-project laravel/laravel bbtoheroku --prefer-dist

    Create proc file in the root of the project (this is for heroku to validate the app)
        $ touch Procfile
    
    Add this text in the file
        web: vendor/bin/heroku-php-apache2 public/
    
    Initialize project into git and push to bitbucket repository


Step 2: Create Heroku App

    After creating heroku app go back to bitbucket and set bitbucket repository variables
    Go to your bitbucket project settings/repository variable
        1. HEROKU_API_KEY
        2. HEROKU_APP_NAME

Step 3: Create pipeline

    Enable pipeplines under bitbucket project settings/deployments
    Just copy the bitbucket-pipelines.yml here

Step 4: Add heroku APP_KEY authentication to your project

    Under your project folder using terminal
        $ heroku login
    After login add the auth key
        $ heroku config:set APP_KEY= <APP_KEY inside found laravel .env file> --app <heroku-app-name>
            example:
                $ heroku config:set APP_KEY=base64:H3gP1tDLGgvB5oBEZ51pv/SkutaBSGnDPai/kWmXi5Y= --app bbtoheroku


reference:
Laravel to Heroku
https://appdividend.com/2018/04/17/how-to-deploy-laravel-project-on-heroku/

Bitbucket to Heroku
https://medium.com/@manajitpal/auto-deployment-using-bitbucket-and-heroku-521b4271cc27
    